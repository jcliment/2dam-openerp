from openerp.osv import fields,osv

class academy_wizard(osv.osv_memory):
    _name = 'academy.wizard'

    _columns = {
        'classroom_origen': fields.integer('Origen', required=True),
        'classroom_update': fields.integer('Update', required=True),
    }

    def update_classrooms(self, cr, uid, ids, data, context={}):
        form = self.browse(cr, uid, ids[0])
        classroom_origen = form.classroom_origen
        classroom_update = form.classroom_update

        classroom_obj = self.pool.get('academy.classroom')
        classroom_ids = classroom_obj.search(cr, uid, [('capacity','=',classroom_origen)])

        values = {
            'capacity':classroom_update,
        }
        classroom_obj.write(cr, uid, classroom_ids, values)
        
        context.update({'classroom_info_updates': len(classroom_ids)})
        
        return {
             'type': 'ir.actions.act_window',
             'name': 'Update classrooms result',
             'res_model': 'academy.wizard.step2', 
             'view_type': 'form',
             'view_mode': 'form',
             'view_id': 0,
             'target': 'new',
             'context' : context,
         }

academy_wizard()

class academy_wizard_step2(osv.osv_memory):
    _name = 'academy.wizard.step2'

    _columns = {
        'classroom_info_updates': fields.integer('Info Updates', readonly=True),
    }
    
    def default_get(self, cr, uid, fields, context=None):
        data = super(academy_wizard_step2, self).default_get(cr, uid, fields, context=context)
        data['classroom_info_updates'] = context.get('classroom_info_updates', False)
        return data
academy_wizard_step2()